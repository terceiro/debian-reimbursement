from datetime import date

from django import forms
from django.conf import settings
from django.contrib.auth.models import Group
from django.utils.translation import gettext as _

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset
from historical_currencies.choices import currency_choices
from historical_currencies.formatting import render_amount

from reimbursements.models import Receipt, ReceiptLine, Request, RequestLine
from reimbursements.widgets import NativeDateInput
from reimbursements.fields import RequiredCountries


class BankDetailsForm(forms.Form):
    recipient_name = forms.CharField(label=_("Name"))
    email = forms.EmailField(label=_("Email"))
    address_firstLine = forms.CharField(label=_("Address Line 1"), required=False)
    address_secondLine = forms.CharField(label=_("Address Line 2"), required=False)
    address_city = forms.CharField(label=_("City"), required=False)
    address_state = forms.CharField(label=_("State"), required=False)
    address_postCode = forms.CharField(label=_("Postal Code"), required=False)
    address_country = forms.ChoiceField(label=_("Country"), choices=RequiredCountries())
    bank_name = forms.CharField(label=_("Bank Name"), required=False)
    accountNumber = forms.CharField(label=_("Account Number"))
    iban = forms.CharField(label=_("IBAN"), required=False)
    bank_branch_code = forms.CharField(label=_("Bank Branch Code"), required=False)
    bic = forms.CharField(label=_("SWIFT/BIC Code"), required=False)
    bank_address_firstLine = forms.CharField(
        label=_("Bank Address Line 1"), required=False
    )
    bank_address_secondLine = forms.CharField(
        label=_("Bank Address Line 2"), required=False
    )
    bank_address_city = forms.CharField(label=_("Bank City"), required=False)
    bank_address_state = forms.CharField(label=_("Bank State"), required=False)
    bank_address_postCode = forms.CharField(label=_("Bank Postal Code"), required=False)
    bank_address_country = forms.ChoiceField(
        label=_("Bank Country"),
        choices=RequiredCountries(),
        required=False,
    )

    def __init__(self, initial=None, instance=None, **kwargs):
        object_data = {}
        self.instance = instance
        if instance and instance.bank_details:
            object_data = instance.bank_details
        if initial is not None:
            object_data.update(initial)
        super().__init__(initial=object_data, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def full_clean(self):
        super().full_clean()
        if hasattr(self, "cleaned_data"):
            self.instance.bank_details = self.cleaned_data

    def save(self, commit=True):
        if self.errors:
            raise ValueError("Could not save with errors")
        if commit:
            self.instance.save()
        return self.instance


class RequestForm(forms.ModelForm):
    class Meta:
        model = Request
        fields = ("request_type", "description", "currency")
        widgets = {
            "currency": forms.Select(choices=currency_choices()),
        }
        help_texts = {
            "description": _(
                "Describe the request in a sentence or two. "
                "First line is the subject (like a git commit)."
            ),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def save(self, commit=True):
        instance = super().save(commit=False)
        if instance._state.adding:
            instance.approver_group = Group.objects.get(name=settings.DEFAULT_APPROVER)
            instance.payer_group = Group.objects.get(name=settings.DEFAULT_PAYER)
            instance.requester = self.request_user
        if commit:
            instance.save()
        return instance


class RequestLineForm(forms.ModelForm):
    class Meta:
        model = RequestLine
        widgets = {
            "description": forms.TextInput(),
        }
        exclude = ()


class RequestLineFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.template = f"{settings.CRISPY_TEMPLATE_PACK}/table_inline_formset.html"


RequestLineFormset = forms.models.inlineformset_factory(
    Request, RequestLine, form=RequestLineForm, exclude=(), extra=1
)


class RequestSubmitForm(forms.Form):
    compliant = forms.BooleanField(
        label=_(
            "The expenses I see reimbursement for are compliant with the "
            "policies of the project and the fiscal sponsor."
        )
    )
    sole_source = forms.BooleanField(
        label=_(
            "I have not sought nor will seek reimbursement of these "
            "expenses from any other source."
        )
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False


class RequestReimbursementForm(forms.Form):
    details = forms.CharField(
        help_text=_("An optional comment to attach to the reimbursement"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 3}),
    )
    fee = forms.DecimalField(
        help_text=_("Reimbursement Cost: Bank transfer fees etc."),
        required=False,
    )
    fee_currency = forms.ChoiceField(
        choices=currency_choices(),
        help_text=_("Reimbursement Cost: Currency"),
        required=False,
    )
    fee_details = forms.CharField(
        help_text=_("An optional comment to attach to the fees"),
        required=False,
        widget=forms.Textarea(attrs={"rows": 3}),
    )

    def __init__(self, request, **kwargs):
        super().__init__(**kwargs)
        self.type_fields = []
        for type_id, summary in request.summarize_by_type().items():
            if type_id == "total":
                continue
            default = summary.reimburseable
            field = forms.DecimalField(
                label=summary.name,
                initial=default,
                help_text=_("Budgeted: %s, Spent: %s, Approved up to: %s")
                % (
                    render_amount(*summary.qualified_requested),
                    render_amount(*summary.qualified_spent),
                    render_amount(*summary.qualified_approved),
                ),
                widget=forms.widgets.NumberInput(attrs={"class": "sum-line"}),
            )
            self.fields[str(type_id)] = field
            self.type_fields.append(str(type_id))
        self.fields["fee_currency"].initial = request.currency
        self.helper = FormHelper(self)
        self.helper.form_tag = False
        self.helper.layout = Layout(
            Fieldset(_("Expense Types"), *self.type_fields),
            "details",
            "fee",
            "fee_currency",
            "fee_details",
        )

    def clean(self):
        cleaned_data = super().clean()
        fee = cleaned_data.get("fee")
        fee_currency = cleaned_data.get("fee_currency")
        if fee and not fee_currency:
            self.add_error("fee_currency", _("Currency required"))


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ("file", "description")
        help_texts = {
            "description": _(
                "Describe the receipt in a sentence or two. "
                "First line is the subject (like a git commit)."
            ),
        }
        widgets = {
            "description": forms.Textarea(attrs={"rows": 3}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_tag = False

    def save(self, commit=True):
        instance = super().save(commit=False)
        if instance._state.adding:
            instance.request = self.request
        if commit:
            instance.save()
        return instance


class ReceiptLineForm(forms.ModelForm):
    class Meta:
        model = ReceiptLine
        widgets = {
            "currency": forms.Select(choices=currency_choices()),
            "description": forms.TextInput(),
            "date": NativeDateInput(
                attrs={
                    "max": date.today,
                }
            ),
        }
        exclude = ["request", "converted_amount"]


class ReceiptLineFormHelper(FormHelper):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.form_tag = False
        self.template = f"{settings.CRISPY_TEMPLATE_PACK}/table_inline_formset.html"


class DefaultCurrencyInlineFormset(forms.models.BaseInlineFormSet):
    """
    Formset with a default currency configured through a default_currency attribute
    """

    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)
        is_empty_form = index is None  # the default form copied by JS
        is_new_instance = self.instance.pk is None
        if is_empty_form or is_new_instance:
            kwargs.setdefault("initial", {}).setdefault(
                "currency", self.default_currency
            )
        return kwargs


ReceiptLineFormset = forms.models.inlineformset_factory(
    Receipt,
    ReceiptLine,
    extra=1,
    form=ReceiptLineForm,
    formset=DefaultCurrencyInlineFormset,
)
ReceiptLineEditFormset = forms.models.inlineformset_factory(
    Receipt,
    ReceiptLine,
    extra=0,
    form=ReceiptLineForm,
    formset=DefaultCurrencyInlineFormset,
)
