{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request reimbursed: {{ subject }}
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with full_name=object.requester.get_full_name payer_group=object.payer_group.name request_url=object.get_fully_qualified_url subject=object.subject reimbursed_total=object.reimbursed_total|currency %}
Dear {{ full_name }},

Your request for {{ subject }}
has been reimbursed by {{ payer_group }}.
This may take a couple of days to complete.

The amount reimbursed is {{ reimbursed_total }}.
You can review the full request here:
{{ request_url }}

Thank you,

The {{ SITE_NAME }} treasurers
{% endblocktrans %}
{% endblock %}
