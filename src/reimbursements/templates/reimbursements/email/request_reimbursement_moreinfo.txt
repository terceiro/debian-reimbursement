{% load i18n %}
{% load currency_format %}
{% block subject %}
{% blocktrans with subject=object.subject %}
{{ SITE_NAME }}: Request {{ subject }} requires more information
{% endblocktrans %}
{% endblock %}
{% block body %}
{% blocktrans with subject=object.subject full_name=object.requester.get_full_name total=object.total|currency request_url=object.get_fully_qualified_url %}
Dear {{ full_name }},

Before your request for {{ total }} for {{ subject }}
can be reimbursed, some more information is required:

{{ details }}

Please provide the information requested, and resubmit the request:
{{ request_url }}

Thank you,

The {{ SITE_NAME }} treasurers
{% endblocktrans %}
{% endblock %}
