from unittest import mock

from django.conf import settings
from django.test import SimpleTestCase, override_settings

from reimbursements.integrations.irc import IrkerClient, KGBClient


class TestIrkerClientDetection(SimpleTestCase):
    @override_settings()
    def test_detects_no_settings(self):
        del settings.IRKER
        self.assertFalse(IrkerClient.detect())

    def test_detects_settings(self):
        with self.settings(
            IRKER={
                "TARGET": "ircs://irc.debian.org/my-channel",
            }
        ):
            self.assertTrue(IrkerClient.detect())


class TestIrkerClient(SimpleTestCase):
    def setUp(self):
        self.client = IrkerClient()
        self.client.send = mock.Mock()

    def irker_settings(self, **kwargs):
        irker = {"TARGET": "ircs://irc.debian.org/my-channel"}
        irker.update(kwargs)
        return self.settings(IRKER=irker)

    def test_defaults(self):
        with self.irker_settings():
            self.client.notify("hello")
        self.client.send.assert_called_once_with(
            {"to": "ircs://irc.debian.org/my-channel", "privmsg": "hello"},
            server=("localhost", 6659),
            timeout=5,
        )

    def test_notice(self):
        with self.irker_settings(MESSAGE_TYPE="notice"):
            self.client.notify("hello")
        self.client.send.assert_called_once_with(
            {"to": "ircs://irc.debian.org/my-channel", "notice": "hello"},
            server=("localhost", 6659),
            timeout=5,
        )

    def test_timeout(self):
        with self.irker_settings(TIMEOUT=10):
            self.client.notify("hello")
        self.client.send.assert_called_once_with(
            {"to": "ircs://irc.debian.org/my-channel", "privmsg": "hello"},
            server=("localhost", 6659),
            timeout=10,
        )

    def test_server(self):
        with self.irker_settings(SERVER=("example.com", 1234)):
            self.client.notify("hello")
        self.client.send.assert_called_once_with(
            {"to": "ircs://irc.debian.org/my-channel", "privmsg": "hello"},
            server=("example.com", 1234),
            timeout=5,
        )


# TODO: Test IrkerClient.send()


@mock.patch("reimbursements.integrations.irc.binary_in_path")
class TestKGBClientDetection(SimpleTestCase):
    @override_settings()
    def test_detects_no_settings(self, binary_in_path):
        del settings.KGB_CONFIG
        binary_in_path.return_value = True
        self.assertFalse(KGBClient.detect())

    def test_detects_settings(self, binary_in_path):
        binary_in_path.return_value = True
        with self.settings(KGB_CONFIG="/etc/kgb-client.conf"):
            self.assertTrue(KGBClient.detect())

    def test_requires_binary(self, binary_in_path):
        binary_in_path.return_value = False
        with self.settings(KGB_CONFIG="/etc/kgb-client.conf"):
            self.assertFalse(KGBClient.detect())
